#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <wchar.h>
#include <locale.h>
#include <unistd.h>

#define CONSOLE_UNDERLINE "\e[4m"
#define CONSOLE_NORMAL "\e[0m"
#define CONSOLE_BG_DARK "\e[40m"

const char *help =
"Usage : %s [--id ID] [--modify FIELD VALUE]\n"
"\n"
"Selecting :\n"
"--id, -i [ID]              : Selects an id.\n"
"\n"
"Operations :\n"
"--modify, -m [FIELD VALUE] : Modifies field FIELD of selected id with new value VALUE.\n"
"                             If value is empty, deletes field.\n"
"\n"
"By default, shows a list of all contacts.\n"
"Contacts are stored in ~/.contacts/contacts.csv\n"
;

wchar_t *default_keys[] = {
	L"First Name", L"Last Name", L"Mobile Phone", L"Birthday"
};

#define LEN_DEFAULT_KEYS (sizeof(default_keys)/sizeof(*default_keys))

struct contacts 
{
	wchar_t **headers;
	wchar_t ***rows;
	long int n_rows;
	int n_fields;
};

struct arg_display_contacts {
	wchar_t **keys;
	int count;
};

struct arg_modify_contact {
	long int *id;
	wchar_t *field;
	wchar_t *newvalue;
};

struct {
	bool color;
} config;

long int count_csv_lines(FILE *f)
{
	long int ret = 0;
	bool in_quotes = false;
	for (wint_t c = fgetwc(f); feof(f) == 0; c = fgetwc(f)) {
		if (c == WEOF) {
			perror("fgetwc");
		}
		if (c == L'"') {
			in_quotes = !in_quotes;
		}
		if (c == L'\n') {
			if (!in_quotes) {
				ret++;
			}
		}
	}
	return ret;
}

size_t parse_csv_line(FILE *f, wchar_t **out, int n_fields, wchar_t delimiter)
{
	wchar_t line[8 * 1024]; // Hope 8K'll be enough.

	wchar_t *oldcurs = line;
	wchar_t *curs;
	bool in_quotes = false;
	fgetws(line, sizeof(line), f);
	for (int i = 0; i < n_fields; i++) {
		for (curs = oldcurs; !(!in_quotes && (*curs == delimiter || *curs == '\n')); curs++) {
			if (*curs == L'\0') {
				if (in_quotes) {
					if (fgetws(curs, sizeof(line) - (curs - oldcurs), f) == NULL) {
						fwprintf(stderr, L"File ended while in a quoted expression !\n");
						exit(-1);
					}
				}
				else {
					break;
				}
			}
			if (*curs == L'"') {
				in_quotes = !in_quotes;
			}
		}

		size_t len = (size_t)(curs - oldcurs);
		if (len >= 1) {
			out[i] = malloc((len + 1) * sizeof(wchar_t));
			memcpy(out[i], oldcurs, len * sizeof(wchar_t));
			out[i][len] = '\0';
		}

		oldcurs = curs + 1;
	}

	return wcslen(line);
}

void parse_csv(FILE *f, struct contacts *contacts)
{
	// Header :
	//	Count number of fields in line :
	contacts->n_fields = 1;
	for (wint_t c = fgetwc(f); c != '\n'; c = fgetwc(f)) {
		if (c == L',') {
			contacts->n_fields++;
		}
		else if (c == L'\n') {
			break;
		}
	}

	rewind(f);

	//	Fill header row names :
	contacts->headers = calloc(contacts->n_fields, sizeof(wchar_t *));

	size_t header_len = parse_csv_line(f, contacts->headers, contacts->n_fields, ',');

	// Body :
	// 	First, count number of rows :
	contacts->n_rows = count_csv_lines(f);
	contacts->rows = calloc(contacts->n_rows, sizeof(wchar_t **));

	//	Rewind file :
	if (fseek(f, header_len, SEEK_SET) < 0) {
		perror("fseek");
	}

	//	Fill table :
	for (long int i = 0; i < contacts->n_rows; i++) {
		contacts->rows[i] = calloc(contacts->n_fields, sizeof(wchar_t *));
		parse_csv_line(f, contacts->rows[i], contacts->n_fields, ',');
	}
}

void free_contacts(struct contacts *c) 
{
	for (int i = 0; i < c->n_fields; i++) {
		if (c->headers[i]) {
			free(c->headers[i]);
		}
	}
	if (c->headers) {
		free(c->headers);
	}

	for (int i = 0; i < c->n_rows; i++) {
		for (int j = 0; j < c->n_fields; j++) {
			if (c->rows[i][j]) {
				free(c->rows[i][j]);
			}
		}
		if (c->rows[i]) {
			free(c->rows[i]);
		}
	}
	if (c->rows) {
		free(c->rows);
	}

	free(c);
}

/** \brief Displays contacts from c using headers from keys
 *
 * \param c	Contacts list to display.
 * \param keys	Array of strings to match with the contacts headers.
 * \param count	Number of elements in keys.
 */
void display_contacts(struct contacts *c, wchar_t **keys, int count)
{
	// Get indices of matching header entries :
	int matched = 0;
	long int *indices = calloc(count, sizeof(long int));
	for (int i = 0; i < count; i++) {
		for (int j = 0; j < c->n_fields; j++) {
			if(wcscmp(keys[i], c->headers[j]) == 0) {
				indices[matched++] = j;
				continue;
			}
		}
	}

	// Calculate column widths :
	size_t *widths = calloc(matched, sizeof(size_t));

	for (int i = 0; i < matched; i++) {
		widths[i] = wcslen(c->headers[indices[i]]);
		for (int j = 0; j < c->n_rows; j++) {
			if (c->rows[j][indices[i]]) {
				widths[i] = fmax(widths[i], wcslen(c->rows[j][indices[i]]));
			}
		}
	}

	size_t id_width = ceil(log10(c->n_rows)) + 1;

	// Print headers :
	if (config.color) {
		wprintf(L"%s", CONSOLE_UNDERLINE);
	}

	for (size_t i = wprintf(L"ID"); i < id_width; i++) {
		putwchar(L' ');
	}

	for (int i = 0; i < matched; i++) {
		for (size_t j = wprintf(L"%ls", c->headers[indices[i]]); j < widths[i]; j++) {
			putwchar(L' ');
		}
		putwchar(L' ');
	}
	if (config.color) {
		wprintf(L"%s", CONSOLE_NORMAL);
	}
	putwchar(L'\n');

	// Print all rows :
	for (long int i = 0; i < c->n_rows; i++) {
		if (config.color && i%2) {
			wprintf(L"%s", CONSOLE_BG_DARK);
		}
		for (size_t j = wprintf(L"%ld", i + 1); j < id_width; j++) {
			putwchar(L' ');
		}
		for (int j = 0; j < matched; j++) {
			if (c->rows[i][indices[j]]) {
				for (size_t k = wprintf(L"%ls", c->rows[i][indices[j]]); k < widths[j]; k++) {
					putwchar(L' ');
				}
			}
			else {
				for (size_t k = 0; k < widths[j]; k++) {
					putwchar(L' ');
				}
			}
			putwchar(L' ');
		}
		if (config.color) {
			wprintf(L"%s", CONSOLE_NORMAL);
		}
		putwchar(L'\n');
	}

	free(widths);
	free(indices);
}

void _display_contacts(struct contacts *c, void *args)
{
	display_contacts(c, ((struct arg_display_contacts*) args)->keys, ((struct arg_display_contacts*) args)->count);
}

void display_single_contact(struct contacts *c, long int id)
{
	size_t widths[2] = { wcslen(L"Name"), wcslen(L"Value") };

	if (id > c->n_rows || id < 0) {
		fwprintf(stderr, L"Id %ld is out-of-bounds.\n", id + 1);
		return;
	}

	for (int i = 0; i < c->n_fields; i++) {
		if (c->rows[id][i]) {
			widths[0] = fmax(widths[0], wcslen(c->headers[i]));
			widths[1] = fmax(widths[1], wcslen(c->rows[id][i]));
		}
	}

	wchar_t format[32];
	swprintf(format, 32, L"%%-%ldls %%-%ldls", widths[0] + 1, widths[1] + 1);

	if (config.color) {
		wprintf(L"%s", CONSOLE_UNDERLINE);
	}
	wprintf(format, L"Name", L"Value");
	if (config.color) {
		wprintf(L"%s", CONSOLE_NORMAL);
	}
	putwchar(L'\n');
	int count = 0;
	for (int i = 0; i < c->n_fields; i++) {
		if (c->rows[id][i]) {
			if (config.color && (count++%2)) {
				wprintf(L"%s", CONSOLE_BG_DARK);
			}
			wprintf(format, c->headers[i], c->rows[id][i]);
			if (config.color) {
				wprintf(L"%s", CONSOLE_NORMAL);
			}
			putwchar(L'\n');
		}
	}
}

void _display_single_contact(struct contacts *c, void *args) 
{
	display_single_contact(c, *(long int*) args);
}

int compare_csv(const void *a, const void *b)
{
	const wchar_t * const *A = a;
	const wchar_t * const *B = b;
	int c = 0;

	return wcscmp(A[c], B[c]);
}

void sort_csv(struct contacts *c)
{
	qsort(c->rows, c->n_rows, sizeof(char **), compare_csv);
}

void _parsing_error(__attribute__((unused)) struct contacts *c, __attribute__((unused)) void *args)
{
	fwprintf(stderr, L"Error in parsing..\n");
}

// void (*operations[])(struct contacts *, void *) = { _display_contacts, _display_single_contact };

void output_csv(struct contacts *c, FILE *f) 
{
	int i;
	for (i = 0; i < c->n_fields - 1; i++) {
		fwprintf(f, L"%ls,", c->headers[i]);
	}
	fwprintf(f,L"%ls\n", c->headers[i]);

	for (int j = 0; j < c->n_rows; j++) {
		for (i = 0; i < c->n_fields -1; i++) {
			if (c->rows[j][i]) {
				fwprintf(f, L"%ls", c->rows[j][i]);
			}
			fputwc(L',', f);
		}
		if (c->rows[j][i]) {
			fwprintf(f, L"%ls", c->rows[j][i]);
		}
		fputwc(L'\n', f);
	}
}

void _modify_contact(struct contacts *c, void *args) 
{
	struct arg_modify_contact *a = args;
	if (*a->id < 0 || *a->id >= c->n_rows) {
		fwprintf(stderr, L"Id %ld is out-of-bounds.\n", a->id + 1);
		return;
	}
	if (a->field == NULL || a->newvalue == NULL) {
		fwprintf(stderr, L"modify operation expects a field to modify and a new value to use\n");
		return;
	}

	int field = -1;
	for (int i = 0; i < c->n_fields; i++) {
		if (wcscmp(a->field, c->headers[i]) == 0) {
			field = i;
		}
	}

	if (field == -1) {
		fwprintf(stderr, L"Field %ls not found\n", a->field);
		return;
	}

	if (c->rows[*a->id][field] != NULL) {
		free(c->rows[*a->id][field]);
		c->rows[*a->id][field] = NULL;
	}

	c->rows[*a->id][field] = malloc((wcslen(a->newvalue) + 1) * sizeof(wchar_t));;
	wcscpy(c->rows[*a->id][field], a->newvalue);
}

int main(int argc, char *argv[]) 
{
	for (int i = 1; i < argc; i++) {
		if (strcmp(argv[i], "--help") == 0 || strcmp(argv[i], "-h") == 0) {
			printf(help, argv[0]);
			return 0;
		}
		else if (strcmp(argv[i], "--version") == 0) {
			printf("pre-alpha\n");
			return 0;
		}
	}

	bool modifies = false;
	memset(&config, 0, sizeof(config));
	
	config.color = isatty(1);

	setlocale(LC_ALL, "en_US.UTF-8");
	setlocale(LC_CTYPE, "en_US.UTF-8");

	struct contacts *contacts = calloc(1, sizeof(struct contacts));
	
	// Parsing args :
	void (*selected_op)(struct contacts *, void *) = _parsing_error;
	void *args = NULL;
	struct arg_modify_contact arg_modify_contact = {0};
	long int selected_id = -1;
	if (argc == 1) {
		selected_op = _display_contacts;
		struct arg_display_contacts arg = { default_keys, LEN_DEFAULT_KEYS };
		args = &arg;
	}
	else {
		for (int i = 1; i < argc; i++) {
			if (strcmp(argv[i], "--id") == 0 || strcmp(argv[i], "-i") == 0) {
				if (i + 1 >= argc) {
					fwprintf(stderr, L"Expected an integer argument after flag %s (id). Exiting.\n", argv[i]);
					return 1;
				}
				i++;
				if (sscanf(argv[i], "%ld", &selected_id) == 1) {
					selected_id--;
					if (selected_op == _parsing_error) {
						selected_op = _display_single_contact;
						args = &selected_id;
					}
				}
				else {
					fwprintf(stderr, L"%s is not an integer\n", argv[i]);
					return 1;
				}
			}
			else if (strcmp(argv[i], "--modify") == 0 || strcmp(argv[i], "-m") == 0) {
				if (i + 2 >= argc) {
					fwprintf(stderr, L"Expected a field name and a new value after flag %s (modify). Exiting.\n", argv[i]);
					return 1;
				}
				i++;
				if (selected_op == _parsing_error || selected_op == _display_single_contact) {
					selected_op = _modify_contact;
				}
				else {
					fwprintf(stderr, L"Got different operation flags. Exiting.\n");
					return -1;
				}
				arg_modify_contact.id = &selected_id;
				size_t len1 = mbstowcs(NULL, argv[i], 0) + 1;
				size_t len2 = mbstowcs(NULL, argv[i + 1], 0) + 1;
				arg_modify_contact.field = malloc(len1 * sizeof(wchar_t));
				arg_modify_contact.newvalue = malloc(len2 * sizeof(wchar_t));
				mbstowcs(arg_modify_contact.field, argv[i], len1);
				mbstowcs(arg_modify_contact.newvalue, argv[++i], len2);
				modifies = true;
				args = &arg_modify_contact;
			}
		}
	}

	char *home = getenv("HOME");
	char path[256];
	snprintf(path, sizeof(path), "%s/.contacts/contacts.csv", home); /**< \todo add support for user-provided path/env-defined path. */
	FILE *f = fopen(path, (modifies) ? "r+" : "r");
	if (f == NULL) {
		perror(path);
		free(contacts);
		return -1;
	}

	parse_csv(f, contacts);

	sort_csv(contacts);

	selected_op(contacts, args);
	if (modifies) {
		rewind(f);
		output_csv(contacts, f);
	}

	fclose(f);
	free_contacts(contacts);
	
	return 0;
}
