# contactty

`contactty` is a lightweight terminal application used to display and modify a contacts database. The database is stored as a csv file.

It makes little assumptions about which fields your csv contains, or in which order they appear. Though, for the time being, everything has only been tested with Google Contacts' "csv for Outlook" export function.

It's still a very young project. Be patient; if you want to help and/or suggest features, shoot me a message !

## Defaults

By default, `contactty` pulls contacts from _~/.contacts/contacts.csv_. There is currently no way to change this default (yet !).

## Compilation

There are currently 2 supported `cmake` targets : release and debug. For the time being, there are virtually no difference between the two, except for the `-g` flag.

### Release

From the top-level directory :

```
$ cmake -DCMAKE_BUILD_TYPE=Release -S . -B release
$ cmake --build release
```

Output can be found at _bin/contactty_.

### Debug

From the top-level directory :

```
$ cmake -DCMAKE_BUILD_TYPE=Debug -S . -B debug
$ cmake --build debug
```

Output can be found at _bin/contactty-dbg_.
